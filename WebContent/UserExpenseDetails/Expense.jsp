<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List, com.model.Expenses"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Expense Tracker | Details</title>
<style><%@include file="ExpenseStyle.css"%></style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%
		String userName = (String)session.getAttribute("userName");
	%>
	<div class="nav-bar">
		<h3>Expense Tracker</h3>
		<p>${ userName }</p>
	</div>
	<div class="home">
		<div class="details">
			<h3>List</h3>
			<c:if test="${exDetails == null}">  
   				<p> No details found!<p>  
			</c:if>
			<c:if test="${exDetails.size() == 0}">  
   				<p> No details found!<p>  
			</c:if>
			<c:if test="${exDetails != null}">  
					<c:forEach var="ex" items="${exDetails}">
					<div class="details-div"> 
						<div class="sub-details-div">
							<p>${ex.title}</p>
							<p>&#8377;${ex.amount}</p>
						</div>
						<p>${ex.descri}</p>
					</div>
					</c:forEach>
			</c:if>
		</div>
		<div class="form">
			<h3>Add</h3>
			<form action="/ExpenseTracker/HandleAdd" method="post">
				<input name="title" type="text" placeholder="Title">
				<textarea name="description" rows="5" cols="20" placeholder="Description"></textarea>
				<input name="amount" type="number" placeholder="Amount">
				<button type="submit">Add</button>
				<a href="/ExpenseTracker/UserExpenseDetails/DeleteExpense.html">Delete</a>
				<a href="/ExpenseTracker/UserExpenseDetails/UpdateExpense.html">Update</a>
			</form>
		</div>
	</div>
</body>
</html>