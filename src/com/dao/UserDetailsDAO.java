package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.GetConnection;
import com.model.UserDetails;

public class UserDetailsDAO {
	
	public int getLastId() {
		Connection con = GetConnection.getDBConnection();

		try {
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT MAX(userId) FROM user_details");

			if (res.next()) {
				return res.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public int addUserDetails(int userId, String userName, String mail, String password){
		Connection con = GetConnection.getDBConnection();
		
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO user_details VALUES(?, ?, ?, ?)");
			ps.setInt(1, userId);
			ps.setString(2, password);
			ps.setString(3, mail);
			ps.setString(4, userName);
			
			
			int res = ps.executeUpdate();
			return res;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public UserDetails getDtails(String mail){
		Connection con = GetConnection.getDBConnection();
		UserDetails details = new UserDetails();
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM user_details WHERE mail=?");
			ps.setString(1, mail);
			
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				details.setUserId(rs.getInt("userId"));
				details.setUserName(rs.getString("userName"));
				details.setMail(rs.getString("mail"));
				details.setPassword(rs.getString("password"));
				
				return details;
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}
}
