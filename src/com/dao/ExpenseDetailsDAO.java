package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.db.GetConnection;
import com.model.Expenses;

public class ExpenseDetailsDAO {
	
	public int updateExpense(int amount, int userId, String title){
		Connection con = GetConnection.getDBConnection();
		try {
			PreparedStatement ps = con.prepareStatement("UPDATE expenses SET amount=? WHERE userId=? and title=?");
			ps.setInt(1, amount);
			ps.setInt(2, userId);
			ps.setString(3, title);
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public int deleteExpense(String title, int userId){
		Connection con = GetConnection.getDBConnection();
		try {
			PreparedStatement ps = con.prepareStatement("DELETE FROM expenses WHERE title=? and userId=?");
			ps.setString(1, title);
			ps.setInt(2, userId);
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	
	public int addExpenseDetails(int userId, String title, String descri, int amount) {
		Connection con = GetConnection.getDBConnection();

		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO expenses VALUES(?, ?, ?, ?)");
			ps.setInt(1, userId);
			ps.setString(2, title);
			ps.setString(3, descri);
			ps.setInt(4, amount);

			int res = ps.executeUpdate();
			return res;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<Expenses> getDetails(int userId) {
		List<Expenses> list = new LinkedList<>();
		Connection con = GetConnection.getDBConnection();
		

		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM expenses WHERE userId=?");
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Expenses details = new Expenses();
				details.setUserId(rs.getInt("userId"));
				details.setTitle(rs.getString("title"));
				details.setDescri(rs.getString("descri"));
				details.setAmount(rs.getInt("amount"));
				
				list.add(details);
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
