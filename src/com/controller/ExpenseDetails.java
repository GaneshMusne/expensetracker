package com.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ExpenseDetailsDAO;
import com.dao.UserDetailsDAO;
import com.model.Expenses;
import com.model.UserDetails;

@WebServlet("/ExpenseDetails")
public class ExpenseDetails extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String mail = request.getParameter("mail");
		String password = request.getParameter("password");

		UserDetailsDAO dao = new UserDetailsDAO();
		UserDetails ud = dao.getDtails(mail);
		PrintWriter pt = response.getWriter();

		if (ud != null) {
			if (password.equals(ud.getPassword())) {
				ExpenseDetailsDAO exDao = new ExpenseDetailsDAO();
				List<Expenses> exDetails = exDao.getDetails(ud.getUserId());
				HttpSession session = request.getSession();  
		        session.setAttribute("userId",ud.getUserId());
				session.setAttribute("userName", ud.getUserName());
				request.setAttribute("exDetails", exDetails);
				RequestDispatcher rd = request.getRequestDispatcher("/UserExpenseDetails/Expense.jsp");
				rd.forward(request, response);
				
			} else {
				pt.write("<html><head>");
				pt.write("<center>");
				pt.write("<h1>Please Enter Valid password</h1>");
				pt.write("<a href='/ExpenseTracker/Login/Login.html' style='margin-right: 1rem;'>Login</a>");
				pt.write("<a href='/ExpenseTracker/Home/Home.html'>Home</a>");
				pt.write("</center>");
				pt.write("</body></html>");
			}
		} else {
			pt.write("<html><head>");
			pt.write("<center>");
			pt.write("<h1>Please Enter Valid Mail</h1>");
			pt.write("<a href='/ExpenseTracker/Login/Login.html' style='margin-right: 1rem;'>Login</a>");
			pt.write("<a href='/ExpenseTracker/Home/Home.html'>Home</a>");
			pt.write("</center>");
			pt.write("</body></html>");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
