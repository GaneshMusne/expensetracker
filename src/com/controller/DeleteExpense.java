package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ExpenseDetailsDAO;
import com.model.Expenses;

/**
 * Servlet implementation class DeleteExpense
 */
@WebServlet("/DeleteExpense")
public class DeleteExpense extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("title");
		ExpenseDetailsDAO exDao = new ExpenseDetailsDAO();
		HttpSession session=request.getSession(false);  
        int userId = (Integer)session.getAttribute("userId");
		int res = exDao.deleteExpense(title, userId);
		ExpenseDetailsDAO dao = new ExpenseDetailsDAO();
        List<Expenses> exDetails = dao.getDetails(userId);
        request.setAttribute("exDetails", exDetails);
		if(res == 1){
	        RequestDispatcher rd = request.getRequestDispatcher("/UserExpenseDetails/Expense.jsp");
			rd.include(request, response);
		} else {
			PrintWriter pt = response.getWriter();
			pt.write("<html><head>");
			pt.write("<center>");
			RequestDispatcher rd = request.getRequestDispatcher("/UserExpenseDetails/Expense.jsp");
			rd.include(request, response);
			pt.write("<h1>Somthing went wrong Try again</h1>");
//			pt.write("<a href='/ExpenseTracker/UserExpenseDetails/Expense.jsp'>Home</a>");
			pt.write("</center>");
			pt.write("</body></html>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
