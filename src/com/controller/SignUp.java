package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDetailsDAO;

@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		String mail = request.getParameter("mail");
		String password = request.getParameter("password");
		
		
		UserDetailsDAO dao = new UserDetailsDAO();
		int userId = dao.getLastId();
		
		if(userId == 0) {
			userId = 101;
		} else {
			userId += 1;
		}
		
		int res = dao.addUserDetails(userId, userName, mail, password);
		
		PrintWriter pt = response.getWriter();
		pt.write("<html><head>");
		pt.write("<center>");
		if(res != 0){
			pt.write("<h1>Registration successfull...</h1>");
			pt.write("<a href='/ExpenseTracker/Login/Login.html' style='margin-right: 1rem;'>Login</a>");
			pt.write("<a href='/ExpenseTracker/Home/Home.html'>Home</a>");
		} else {
			pt.write("<h1>Registration Failed...</h1>");
		}
		pt.write("</center>");
		pt.write("</body></html>");
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
